-- [SECTION] Inserting Records
INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("MYMP");

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
    "Beyond Acoustic",
    "2005-1-15",
    2
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
    "Trip",
    "1996-1-15",
    1
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Love Moves (In Mysterious Ways)",
    432,
    "OPM",
    2
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Kundiman",
    239,
    "OPM",
    1
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Kisapmata",
    259,
    "OPM",
    1
);

-- [SECTION] Selecting Records

-- Display all data for all songs
SELECT * FROM songs; 

-- Display only the title and genre of all songs
SELECT song_name, genre FROM songs;

-- Display only the title of all OPM songs
SELECT song_name FROM songs WHERE genre = "OPM";

-- Display the title and length of OPM songs that are more than 4 minutes
SELECT song_name, length FROM songs WHERE length > 400 AND genre = "OPM";

-- We can use both the AND and OR keywords for multiple expressions in the WHERE clause.

-- [SECTION] Updating records

-- Update the length of Kundiman to 220 seconds
UPDATE songs SET length = 220 WHERE song_name = "Kundiman";

-- Ommitting the WHERE clause will update ALL rows

-- [SECTION] Deleting records

-- Delete all OPM songs that are longer than 4 minutes
DELETE FROM songs WHERE genre = "OPM" AND length > 240;